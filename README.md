# Demo for spring

## IDE

- Recomment using [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download/#section=windows)

## Tool

- [Spring Initilizer](https://start.spring.io/)
- [Learn Spring Boot](https://viblo.asia/p/hoc-spring-boot-bat-dau-tu-dau-6J3ZgN7WKmB)
- [Lombok](https://loda.me/general-huong-dan-su-dung-lombok-giup-code-java-nhanh-hon-69-loda1552789752787/)
- [Spring Data JPA @Query](https://www.baeldung.com/spring-data-jpa-query)
- [DAO & Repository](https://www.baeldung.com/java-dao-vs-repository)
- [Spring REST API with Swagger2](https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)
- [Model mapper](https://viblo.asia/p/su-dung-modelmapper-trong-spring-boot-63vKj1Vd52R)
- [Custom Serilizer](https://www.baeldung.com/jackson-custom-serialization)
- [Stream API](https://www.baeldung.com/java-8-streams)

## Check Maven version & packages

- [Check maven repository](https://mvnrepository.com/)
- [Alternative maven repository](https://search.maven.org/)
