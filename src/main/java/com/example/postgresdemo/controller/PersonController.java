package com.example.postgresdemo.controller;

import com.example.postgresdemo.entity.Person;
import com.example.postgresdemo.model.CustomPerson;
import com.example.postgresdemo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class PersonController {

    @Autowired
    PersonService personService;

    @GetMapping("/person")
    public List<Person> findPersonsByFirstname(@RequestParam String firstname) {
        return personService.findPersonsByFirstnameQueryDSL(firstname);
    }

    @GetMapping("/custom-person")
    public CustomPerson findCustomPersonsByFirstname(@RequestParam String firstname) {
        return personService.findCustomPersonFirstnameQueryDSL(firstname);
    }

    @GetMapping("/custom-person-serializer")
    public String serializerDemo() {
        return personService.returnItemSerializer();
    }

//    @PostMapping("/person")
//    public List<Person> findPersonsByFirstname(@Valid @RequestBody Person person) {
//        return personService.findPersonsByFirstnameQueryDSL(person.getFirstname());
//    }
}
