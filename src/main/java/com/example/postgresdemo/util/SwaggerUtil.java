package com.example.postgresdemo.util;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerUtil {
  public static Docket getDocket() {
    return new Docket(DocumentationType.SWAGGER_2)
        .groupName("API")
        .select()
//        .apis(RequestHandlerSelectors.any())
        .apis(RequestHandlerSelectors.basePackage("com.example.postgresdemo.controller"))
        .paths(PathSelectors.any())
        .build();
  }
}
