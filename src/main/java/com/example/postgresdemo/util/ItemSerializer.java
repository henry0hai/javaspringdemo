package com.example.postgresdemo.util;

import com.example.postgresdemo.model.Item;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.io.IOException;

public class ItemSerializer extends StdSerializer<Item> {

  public ItemSerializer() {
    this(null);
  }

  public ItemSerializer(Class<Item> t) {
    super(t);
  }

  @Override
  public void serialize(
      Item value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException, JsonProcessingException {

    jgen.writeStartObject();
    jgen.writeNumberField("id", value.getId());
    jgen.writeStringField("itemName", value.getItemName());
    jgen.writeNumberField("owner", value.getOwner().getId());
    jgen.writeEndObject();
  }
}