package com.example.postgresdemo.config;

import com.example.postgresdemo.util.SwaggerUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class CoreSwaggerConfig {
  @Bean
  public Docket swaggerAll() {
    return SwaggerUtil.getDocket();
  }
}