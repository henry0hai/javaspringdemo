package com.example.postgresdemo.model;

import com.example.postgresdemo.util.ItemSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(using = ItemSerializer.class)
public class Item {
  private int id;
  private String itemName;
  private CustomPerson owner;
}
