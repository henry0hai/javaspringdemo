package com.example.postgresdemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class CustomPerson {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private String firstname;

  @Column
  private String surname;

  @Column
  private String email;

  @Column
  private String address;

  @Column
  private int phoneNumber;
}