package com.example.postgresdemo.service;

import com.example.postgresdemo.entity.Person;
import com.example.postgresdemo.model.CustomPerson;
import com.example.postgresdemo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

  @Autowired
  private PersonRepository personRepository;

  public List<Person> findPersonsByFirstnameQueryDSL(String firstname) {
    return personRepository.findPersonsByFirstnameQueryDSL(firstname);
  }

  public CustomPerson findCustomPersonFirstnameQueryDSL(String firstname) {
    return personRepository.findCustomPersonByFirstnameQueryDSL(firstname);
  }

  public String returnItemSerializer() {
    return personRepository.returnItemSerializer();
  }
}
