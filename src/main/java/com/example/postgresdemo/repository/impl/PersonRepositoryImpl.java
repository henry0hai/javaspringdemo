package com.example.postgresdemo.repository.impl;

import com.example.postgresdemo.entity.Person;
import com.example.postgresdemo.entity.QPerson;
import com.example.postgresdemo.model.CustomPerson;
import com.example.postgresdemo.model.Item;
import com.example.postgresdemo.repository.PersonRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.jpa.impl.JPAQuery;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<Person> findPersonsByFirstnameQueryDSL(String firstname) {
        final JPAQuery<Person> query = new JPAQuery<>(em);
        final QPerson person = QPerson.person;
        return query.from(person).where(person.firstname.eq(firstname)).fetch();
    }

    @Override
    public CustomPerson findCustomPersonByFirstnameQueryDSL(String firstname) {
        final JPAQuery<Person> query = new JPAQuery<>(em);
        final QPerson person = QPerson.person;
        Person aPerson = query.from(person).where(person.firstname.eq(firstname)).fetchFirst();
        CustomPerson customPerson = mapper.map(aPerson, CustomPerson.class);
        return customPerson;
    }

    @Override
    public String returnItemSerializer() {
        Item myItem = new Item(1, "theItem", new CustomPerson(1L,"abc", "eee", "ddsdfs", "aaa@aaa.ccc",
            919111));
        String serialized = "";
        try {
            serialized = new ObjectMapper().writeValueAsString(myItem);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return serialized;
    }
}
