package com.example.postgresdemo.repository;

import com.example.postgresdemo.entity.Person;
import com.example.postgresdemo.model.CustomPerson;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface PersonRepository {
    List<Person> findPersonsByFirstnameQueryDSL(String firstname);

    CustomPerson findCustomPersonByFirstnameQueryDSL(String firstname);

    String returnItemSerializer();
}